Source: biojava4-live
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Olivier Sallou <osallou@debian.org>,
           Pierre Gruet <pgt@debian.org>
Section: java
Priority: optional
Build-Depends: debhelper-compat (= 13),
               ant,
               javahelper
Build-Depends-Indep: libcommons-dbcp-java,
                     libhsqldb-java,
                     libcommons-collections3-java,
                     libcommons-pool-java,
                     libcommons-logging-java,
                     libcommons-math-java,
                     libcommons-cli-java,
                     libguava-java,
                     libcommons-codec-java,
                     libitext5-java,
                     libjmol-java,
                     libvecmath-java,
                     default-jdk-headless,
                     junit4,
                     ant-optional,
                     ant-contrib,
                     libhamcrest-java,
                     libjson-simple-java,
                     liblog4j2-java,
                     libslf4j-java,
                     libxmlunit-java,
                     libjgrapht0.8-java,
                     libjaxb-api-java,
                     libjaxb-java,
                     libnetx-java,
                     default-jdk-doc
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/med-team/biojava4-live
Vcs-Git: https://salsa.debian.org/med-team/biojava4-live.git
Homepage: https://www.biojava.org
Rules-Requires-Root: no

Package: libbiojava4.0-java
Architecture: all
Depends: ${java:Depends},
         ${misc:Depends},
         libcommons-pool-java,
         libcommons-collections3-java,
         libcommons-dbcp-java,
         libcommons-logging-java,
         libhsqldb-java,
         libbytecode-java,
         libjson-simple-java,
         libcommons-codec-java,
         libitext5-java,
         libjmol-java,
         liblog4j2-java,
         libguava-java,
         libslf4j-java,
         libcommons-math-java,
         libjgrapht0.8-java,
         libvecmath-java
Suggests: java-virtual-machine,
          libbiojava4-java,
          libbiojava4-java-doc,
          libnetx-java
Description: Java API to biological data and applications (version 4)
 This package presents the Open Source Java API to biological databases
 and a series of mostly sequence-based algorithms.
 .
 BioJava is an open-source project dedicated to providing a Java framework
 for processing biological data. It includes objects for manipulating
 sequences, file parsers, server support, access to BioSQL
 and Ensembl databases, and powerful analysis and statistical routines
 including a dynamic programming toolkit.

Package: libbiojava4-java
Architecture: all
Depends: libbiojava4.0-java,
         ${misc:Depends}
Provides: libbiojava4-java
Description: Java API to biological data and applications (default version)
 BioJava is an open-source project dedicated to providing a Java framework
 for processing biological data. It includes objects for manipulating
 sequences, file parsers, server support, access to BioSQL
 and Ensembl databases, and powerful analysis and statistical routines
 including a dynamic programming toolkit.
 .
 BioJava is provided by a vibrant community which meets annually at
 the Bioinformatics Open Source Conference (BOSC) that traditionally
 accompanies the Intelligent Systems in Molecular Biology (ISMB)
 meeting. Much like BioPerl, the employment of this library is valuable
 for everybody active in the field because of the many tricks of the
 trade one learns just by communicating on the mailing list.
 .
 This is a wrapper package which should enable smooth upgrades to new
 versions.

Package: libbiojava4-java-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends},
         default-jdk-doc
Suggests: libbiojava4-java-demos,
          libbiojava4-java
Description: [Biology] Documentation for BioJava
 BioJava is an open-source project dedicated to providing a Java framework
 for processing biological data.
 .
 This package contains the HTML documentation describing the API of BioJava
 which was generated automatically by JavaDoc.
